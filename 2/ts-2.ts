// type guard
// assertion
// Utility Types

// narrowing
// function padLeft(padding: number | string, input: string): string {
//   if (typeof padding == "number") {
//     return " ".repeat(padding) + input;
//   }

//   return padding + input;
// }

// padLeft(5, "Hello"); // "     Hello"

type Fish = { swim: () => void };
type Bird = { fly: () => void };

function move(animal: Fish | Bird) {
  if ("swim" in animal) {
    return animal.swim();
  }

  return animal.fly();
}

move({ swim: () => {} });

class CustomException extends Error {}

class WkwkException {
  wkwk: number;
}

class InheritWwkwException extends WkwkException {}

const ce = new CustomException();
const iwe = new InheritWwkwException();

function logError(exception: Error | WkwkException) {
  if (exception instanceof Error) {
    console.log(exception.message);
    return;
  }

  console.log(exception.wkwk);
}

// type guard
function isNumber(value: any): value is number {
  return typeof value == "number";
}

function padLeft(padding: number | string, input: string): string {
  if (isNumber(padding)) {
    return " ".repeat(padding) + input;
  }

  return padding + input;
}

class Apa {
  log(b: any): number {
    return this.multBy2(b);
  }

  multBy2(a: number) {
    assertIsNumber(a);

    return a * 2;
  }
}

const ap = new Apa();

ap.log("a");

function assertIsNumber(value: any): asserts value is number {
  if (typeof value != "number") {
    throw Error("Type mismatch");
  }
}

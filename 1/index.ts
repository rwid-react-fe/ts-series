let x: number | string = 1;

x = "hello";
x.toUpperCase();

function anon(a: number) {
 return a;
}

// any spt mematikan fitur type-check
let n: any = 1;

n = "world!";
n.concat(" !"); // world !
anon(n);

let m: unknown = 0;

m = "hello world!";
// m.toUpperCase();
// anon(m);
// `unknown` lebih safe digunakan daripada `any`

interface IPerson {
    SEMESTER: "ganjil" | "genap";
}

interface IPerson {
    NIM: number;
    NAMA: string;
    JENJANG: "S1" | "S2" | "S3"; // S4
}

type Mahasiswa = {
    SEMESTER?: "ganjil" | "genap";
}

type Person = {
    NIM: number;
    NAMA: string;
    JENJANG: "S1" | "S2" | "S3"; // S4
} & Mahasiswa;


const Mahasiswa1: Person = {
    NIM: 235410010,
    NAMA: "John Doe",
    JENJANG: "S3",
}